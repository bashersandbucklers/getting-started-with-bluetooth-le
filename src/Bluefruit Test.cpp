/**
 *	Bluefruit Test
 *
 *	@author Tom Egan <tom@tomegan.tech>
 *	@target Adafruit Flora (flora8)
 *	@dependancy Adafriut NeoPixel (platformio lib ID: 28)
 *	@dependancy Adafriut Bluefruit nRF51  (platformio lib ID: 177)
 *
 *	This project, adapted from Adafuirt Heart Rate Monitor Example, demonstrates
 *	connecting an Adafruit Bluefruit Flora to an Adafruit Flora v3 Arduino
 *	compatible board.
 *
 *	We are using the Arduino framework thus the functions setup and loop are
 *	special if this were a vanilla C/C++ project, one could imagine main being written:
 *
 *	int main() {
 *		Arduino.init();
 *		setup();
 *		while(true) {
 *			loop();
 *		}
 *	}
 */

#include <Arduino.h>
#include <SPI.h>

#include <Adafruit_NeoPixel.h>

#include <Adafruit_BLE.h>
#include <Adafruit_BluefruitLE_UART.h>

/******************************
	Onboard NeoPixel Support
******************************/
#define FLORA_ONBOARD_PIXEL_PIN 8
#define FLORA_ONBOARD_PIXEL_COUNT 1

#define FLORA_ONBOARD_PIXEL_ID 0

Adafruit_NeoPixel strip = Adafruit_NeoPixel(FLORA_ONBOARD_PIXEL_COUNT, FLORA_ONBOARD_PIXEL_PIN, NEO_GRB + NEO_KHZ800);

/******************************
	Bluetooth Support
******************************/
#define BLUEFRUIT_HWSERIAL_NAME      Serial1

//	The blufruit is connected via hardware accelerated serial
Adafruit_BluefruitLE_UART ble(BLUEFRUIT_HWSERIAL_NAME, -1);

// the ids for our BT LE Service and the characteristic where we will be posting data
int32_t serviceId;
int32_t hrmMeasureCharId;
int32_t hrmLocationCharId;


/**
 *	Should a fatal error occur this method will try to output a message
 *	over the (USB) serial port, change the LED color to RED and busy
 *	halt the device
 */
void fatalError(const __FlashStringHelper* err) {
	strip.setBrightness(7);
	strip.setPixelColor(FLORA_ONBOARD_PIXEL_ID, 255, 0, 0);
	strip.show();

	// Wait for serial
	while(!Serial) {
		delay(1000);
	}
	Serial.println(err);
	
	while(1) {
		delay(10000);
	}
}

/**
 *	setup is a special function defined by the Arduino platform
 *	that is called once after the core firmware initialization
 *	happens but before loop is called for the first time
 */
void setup(void) {
	bool success;
	
	randomSeed(micros());
	
	// init the Neopixel
	strip.begin();
	strip.setPixelColor(FLORA_ONBOARD_PIXEL_ID,0,0,255);
	strip.setBrightness(7);
	strip.show();


	// init bluetooth
	success = ble.begin();
	if(!success) {
		fatalError(F("Couldn't find Bluefruit, make sure it's in CoMmanD mode & check wiring?"));
	}

	// perform a factory reset
	success = ble.factoryReset();
	if(!success) {
		fatalError(F("Could not perform factory reset of bluetooth module"));
	}

	// Disable command echo from Bluefruit
	ble.echo(false);
	
	/* Print Bluefruit information */
	if(Serial) {
		ble.info();
	}
	
	// this line is particularly required for Flora, but is a good idea
	// anyways for the super long lines ahead!
	ble.setInterCharWriteDelay(5); // 5 ms

	// Change the device name to make it easier to find
	success = ble.sendCommandCheckOK(F("AT+GAPDEVNAME=Bluefruit HRM"));
	if(!success) {
		fatalError(F("Could not set device name"));
	}
	
	/*
	 *	Add our service definition
	 *
	 *	Services are a bit like a port. Every service operates on a pre-agreed port
	 */
	success = ble.sendCommandWithIntReply(F("AT+GATTADDSERVICE=UUID=0x180D"), &serviceId);
	// A 128bit UUID can be used as below
	//success = ble.sendCommandWithIntReply(F("AT+GATTADDSERVICE=UUID128=00-11-22-33-44-55-66-77-88-99-AA-BB-CC-DD-EE-FF"), &serviceId);
	if(!success) {
		fatalError(F("Could not add service"));
	}

	/* 
	 *	Add the Heart Rate Measurement characteristic
	 *
	 *	Characteristics are a bit like a channels. We could add aditional
	 *	characteristics in the future to the same service
	 *
	 *	PROPERTIES=0x10 signifies that this characteristic can notify
	 */
	success = ble.sendCommandWithIntReply( F("AT+GATTADDCHAR=UUID=0x2A37, PROPERTIES=0x10, MIN_LEN=2, MAX_LEN=3, VALUE=00-40"), &hrmMeasureCharId);
	// A 128bit UUID can be used as below... note that the service and
	// characteristic must not "confilct" at certain octets see the Bluetooth
	// LE documentation for more
	//success = ble.sendCommandWithIntReply(F("AT+GATTADDCHAR=UUID128=00-11-22-33-44-55-66-77-88-99-AA-BB-CC-DD-EE-FF, PROPERTIES=0x10, MIN_LEN=1, MAX_LEN=1, VALUE=0 DESCRIPTION=Strike Notification Channel"), &strikeNotificationCharacteristicId);
	if(!success) {
		fatalError(F("Could not add Heart Rate Measurement characteristic"));
	}

	/* Add the Body Sensor Location characteristic */
	success = ble.sendCommandWithIntReply( F("AT+GATTADDCHAR=UUID=0x2A38, PROPERTIES=0x02, MIN_LEN=1, VALUE=3"), &hrmLocationCharId);
	if(!success) {
		fatalError(F("Could not add Body Sensor Location characteristic"));
	}

	/* Add the Heart Rate Service to the advertising data (needed for Nordic apps to detect the service) */
	ble.sendCommandCheckOK( F("AT+GAPSETADVDATA=02-01-06-05-02-0d-18-0a-18") );

	/* Reset the device for the new service setting changes to take effect */
	ble.reset();
	
	//	We'd really like to use event callbacks for connected and disconnected.
	//	Alas as of Bluefruit firmware v0.8 they still don't work well...
	//	therefore we wait until connected.
	while(!ble.isConnected()) {
		delay(5000);
	}
	
	strip.setPixelColor(FLORA_ONBOARD_PIXEL_ID,0,255,0);
	strip.show();
}

/**
 *	loop is a special function defined by the Arduino platform that is
 *	called continuously in an infinite loop once platform specific
 *	initialization and setup() have run.
 */
void loop(void) {
	int heart_rate = random(50, 100);

	if(Serial) {
		Serial.print(F("Updating HRM value to "));
		Serial.print(heart_rate);
		Serial.println(F(" BPM"));
	}

	strip.setBrightness(10);
	strip.show();
	
	/* Command is sent when \n (\r) or println is called */
	/* AT+GATTCHAR=CharacteristicID,value */
	ble.print( F("AT+GATTCHAR=") );
	ble.print( hrmMeasureCharId );
	ble.print( F(",00-") );
	ble.println(heart_rate, HEX);

	/* Check if command executed OK */
	if( !ble.waitForOK() && Serial ) {
		Serial.println(F("Failed to get response!"));
	}

	delay(100);
	strip.setBrightness(7);
	strip.show();
	
	/* Delay before next measurement update */
	delay(4900);
}
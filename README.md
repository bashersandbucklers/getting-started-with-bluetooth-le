# Getting Started with Bluetooth LE

## About
A simple project setup with platformio for an Adafruit Flora v3 connected to an Adafruit Bluefruit Flora that simulates a Heart Rate Monitor.

## Dependancies
- Adafriut NeoPixel (platformio lib ID: 28)
- Adafriut Bluefruit nRF51  (platformio lib ID: 177)

## Installing
Install on an Adafruit Flora v3 connected with USB to your PC with:

```
platformio run
platformio run -t upload
```